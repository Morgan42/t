<?php

namespace App\Controller;

use App\Entity\Campaign;
use App\Form\CampaignType;
use App\Entity\Payment;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Participant;

/**
 * @Route("/campaign")
 */
class CampaignController extends AbstractController
{

    /**
     * @Route("/create", name="campaign_new", methods="GET|POST")
     */

    public function new(Request $request): Response
    {
        $campaign = new Campaign();
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);
        $campaign->setName($request->request->get('name'));
        if(isset($_GET['campaign_title'])) {
        $campaign_title = $_GET['campaign_title'];
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $id = md5(random_bytes(50));
            $campaign->setId($id);
            $em->persist($campaign);
            $em->flush();

            return $this->redirectToRoute('campaign_show', [
                'id' => $campaign->getId()
            ]);

        }

        if(isset($_GET['campaign_title'])) {
            return $this->render('campaign/new.html.twig', [
                'campaign' => $campaign,
                'form' => $form->createView(),
                'campaign_title' =>$campaign_title,
            ]);
        } else {
            return $this->render('campaign/new.html.twig', [
                'campaign' => $campaign,
                'form' => $form->createView(),
            ]);
        }
    }

    /**
     * @Route("/{id}", name="campaign_show", methods="GET")
     */
    public function show(Campaign $campaign): Response
    {
        $participant = new Participant;

        $query = 'SELECT * FROM participant
        LEFT JOIN payment ON payment.participant_id = participant.id
        WHERE campaign_id = "'.$campaign->getId() .'"' ;
        
        $statement =  $this->getDoctrine()
                            ->getManager()
                            ->getConnection()
                            ->prepare($query);
                            
        $statement->execute();
        $participantWithParticipations = $statement->fetchAll();

        $total_amount = 0;
        $total_participant = count($participantWithParticipations);
        for ($i=0; $i <= ($total_participant - 1) ; $i++)
        {
            $total_amount += $participantWithParticipations[$i]['amount'];
        }
        $total_amount2 = $total_amount / 100;
        $objectif =  $total_amount2 / $campaign->getGoal() * 100;
        return $this->render('campaign/show.html.twig', compact('campaign', 'participantWithParticipations', 'total_participant', 'total_amount2', 'objectif'));
    }    
    
    /**
     * @Route("/{id}/pay", name="campaign_pay", methods="GET|POST")
     */
    public function pay(Request $request, Campaign $campaign): Response
    {

        //$amount_participant = $request->query->get('amount_participant');
        $amount_participant = $_Get['amount_participant'] ?? "";
        return $this->render('campaign/pay.html.twig', compact('campaign', "amount_participant"));
    }

    /**
     * @Route("/{id}/spend", name="campaign_spend", methods="GET|POST")
     */
    public function spend(Request $request, Campaign $campaign): Response
    {
        $amount_participant = $request->query->get('amount_participant');
        return $this->render('campaign/spend.html.twig', compact('campaign', "amount_participant"));
    }

    /**
     * @Route("/{id}/edit", name="campaign_edit", methods="GET|POST")
     */
    public function edit(Request $request, Campaign $campaign): Response
    {
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('campaign_edit', ['id' => $campaign->getId()]);
        }

        return $this->render('campaign/edit.html.twig', [
            'campaign' => $campaign,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="campaign_delete", methods="DELETE")
     */
    public function delete(Request $request, Campaign $campaign): Response
    {
        if ($this->isCsrfTokenValid('delete'.$campaign->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($campaign);
            $em->flush();
        }

        return $this->redirectToRoute('campaign_index');
    }
}
